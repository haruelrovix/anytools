import React from "react";
import ReactDOM from "react-dom";
import Main from "./Main";
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import './style.css';
 
ReactDOM.render(
  <Main/>, 
  document.getElementById("root")
);