import React from "react";

 
class JsonXmlCoverter extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            jsonString: '',
            xmlString: '',
            isCompact: true
        };

    this.handleInputChange = this.handleInputChange.bind(this);
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({
            [name]: value
        });
    }

    convertJsonToXml = () => {
        var convert = require('xml-js');
        var result = ''
        try{
            result = convert.json2xml(this.state.jsonString, 
                {compact: this.state.isCompact, 
                    spaces: 4});
        }catch(err){
            result = err.message
        }
        
        this.setState({
            xmlString: result
        });
    }

    convertXmlToJson = () => {
        var convert = require('xml-js');
        var result = ''
        try{
            result = convert.xml2json(this.state.jsonString, 
                {compact: this.state.isCompact, 
                    spaces: 4});
        }catch(err){
            result = err.message
        }
        this.setState({
            jsonString: result
        });
    }
    
    render() {
        return (
            <div>
            <div>                
                <div className="col-sm-12 col-md-10 pull-left form-inline">    
                    <h2 className="col-sm-12 col-md-10 pull-left">JSON - XML Converter</h2>

                    <div className="col-sm-5 pull-left buffer-half">
                        <h4>JSON</h4>
                        <textarea cols="40" rows="20" 
                            className="font-monospace form-control"
                            name="jsonString" 
                            onChange={this.handleInputChange}
                            value={this.state.jsonString}
                        />
                    </div>

                    <div className="col-sm-2 pull-left buffer-half">
                        <br/><br/><br/><br/>
                    
                        <div className="col-sm-12 col-md-12 pull-left buffer-half">
                        <button onClick={this.convertJsonToXml} className="btn btn-success" type="submit"><span className = "glyphicon glyphicon-arrow-right"></span> </button>
                        </div>
                        <div className="col-sm-12 col-md-12 pull-left buffer-half">
                        <button onClick={this.convertXmlToJson} className="btn btn-success" type="submit"><span className = "glyphicon glyphicon-arrow-left"></span></button>
                        </div>
                        <div className="col-sm-12 col-md-12 pull-left buffer-half">
                            <input type="checkbox" name="isCompact" 
                                checked={this.state.isCompact}
                                onChange={this.handleInputChange} />
                            &nbsp; Compact
                        </div>
                    </div>

                    <div className="col-sm-5 pull-left buffer-half">
                        <h4>XML</h4>
                        <textarea cols="40" rows="20" 
                            className="font-monospace form-control"
                            name="xmlString" 
                            onChange={this.handleInputChange}
                            value={this.state.xmlString}
                        />
                    </div>
                </div>
            </div>
            
            </div>
        );
    }
}
 
export default JsonXmlCoverter