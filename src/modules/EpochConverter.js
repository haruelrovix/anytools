import React from "react";
import moment from "moment";
// import DateTimePicker from 'react-widgets/lib/DateTimePicker'
// import 'react-widgets/dist/css/react-widgets.css'
 
class EpochConverter extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            epochTime: this.getCurrentEpochTime(),
            epochDate: '',
            epochGmtTime: '',
            dateDate: new Date(),
            dateEpoch: '',
            dateFormat: 'ddd MM DD YYYY hh:mm:ss A',
            currentEpochTime: this.getCurrentEpochTime(),
            currentDateTime : new Date(),
        };

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleChange = this.handleChange.bind(this);
    }

    getCurrentEpochTime = () => (
        Math.round((new Date()).getTime() / 1000)
    )

    handleInputChange = (event) => {
        const target = event.target;
        const value = target.value;
        const name = target.name;
        this.setState({
            [name]: value
        });
    }

    handleChange = (value) => {
        this.setState({ dateDate: value });
    }

    saveDate = () => {
        console.log(this.state.dateDate.toString());
    }

    convertEpoch = () => {
        var epoch = this.state.epochTime;
        var date = new Date(epoch * 1000);
        var gmtDate = moment.utc(date);
        this.setState({
            epochDate: date,
            epochGmtTime: gmtDate
        })
    }

    convertDate = () => {        
        var epoch = this.state.dateDate.toString();
        var dateEpoch = Math.round((new Date(epoch)).getTime() / 1000)
        this.setState({
            dateEpoch: dateEpoch
        })
    }
    
    render() {
        return (
            <div>                
                <div className="col-sm-12 col-md-10 pull-left form-group">    
                    <h2 className="col-sm-12 col-md-10 pull-left">Epoch Converter</h2>

                    <div className="col-sm-12 col-md-12 pull-left buffer-half zero-left-right-padding">
                        <div className="col-sm-6 col-md-2 pull-left">
                             Current Epoch Time
                        </div>
                         <div className="col-sm-6 col-md-10 pull-left">
                             &nbsp; {this.state.currentEpochTime.toString()}
                        </div>                        
                    </div>

                    <div className="col-sm-12 col-md-12 pull-left buffer-half zero-left-right-padding">
                        <div className="col-sm-6 col-md-2 pull-left">
                             Current Date Time
                        </div>
                         <div className="col-sm-6 col-md-10 pull-left">
                             &nbsp; {this.state.currentDateTime.toString()}
                        </div>                        
                    </div>

                    <h4 className="col-sm-12 col-md-10 pull-left buffer-top-double">Convert Epoch Time</h4>

                    <div className="col-sm-12 col-md-12 pull-left buffer-half zero-left-right-padding form-inline">
                        <div className="col-sm-3 col-md-2">
                            <span> Epoch Time </span>
                        </div>
                        <div className="col-sm-3 col-md-3">
                            &nbsp; <input type="number" name="epochTime" 
                            onChange={this.handleInputChange}
                            value={this.state.epochTime}
                            className="form-control"
                        />
                        </div>
                        <div className="col-sm-3 col-md-2">
                            <button onClick={this.convertEpoch} className="btn btn-success" type="submit"> Convert </button>
                        </div>                                                   
                    </div>
                    <div className="col-sm-12 col-md-12 pull-left buffer-half zero-left-right-padding">
                        <div className="col-sm-3 col-md-2">
                            <span> Your Time Zone </span>
                        </div>
                        <div className="col-sm-9 col-md-10">
                           &nbsp; {this.state.epochDate.toString()}
                        </div>                                              
                    </div>
                    <div className="col-sm-12 col-md-12 pull-left buffer-half zero-left-right-padding">
                        <div className="col-sm-3 col-md-2">
                            <span> GMT </span>
                        </div>
                        <div className="col-sm-9 col-md-10">
                           &nbsp; {this.state.epochGmtTime.toString()}
                        </div>                                              
                    </div>

                    <h4 className="col-sm-12 col-md-10 pull-left buffer-top-double">Convert Date Time</h4>

                    <div className="col-sm-12 col-md-12 pull-left buffer-half zero-left-right-padding form-inline">
                        <div className="col-sm-3 col-md-2">
                            <span> Date Time </span>
                        </div>
                        <div className="col-sm-3 col-md-3 zero-left-right-padding">
                            {/* <DateTimePicker
                                value={this.state.dateDate}
                                onChange={value => this.setState({ dateDate: value })}
                                format="ddd MMM DD YYYY"
                                time={true}
                            /> */}
                        </div>
                        <div className="col-sm-3 col-md-2">
                            <button onClick={this.convertDate} className="btn btn-success" type="submit"> Convert </button>
                        </div>                                                   
                    </div>     
                    <div className="col-sm-12 col-md-12 pull-left buffer-half zero-left-right-padding">
                        <div className="col-sm-3 col-md-2">
                            <span> Epoch </span>
                        </div>
                        <div className="col-sm-9 col-md-10">
                            &nbsp; {this.state.dateEpoch.toString()}
                        </div>                                              
                    </div>               
                </div>

            </div>

                
        );
    }
}
 
export default EpochConverter