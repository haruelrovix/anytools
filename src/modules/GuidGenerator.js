import React from "react";
 
class GuidGenerator extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isUpperCase: false,
            isHyphens: true,
            numberOfGuids: 1,
            isDoubleQuote: false,
            isBraces: false,
            isComma: false,
            result: ''
        };

    this.handleInputChange = this.handleInputChange.bind(this);
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({
            [name]: value
        });
    }

     handleClick = () => {
        this.generateGuid();
    }

    generateGuid = () => {
        var isHyphens = this.state.isHyphens;
        var isUpperCase = this.state.isUpperCase;
        var numberOfGuids = this.state.numberOfGuids;
        var isDoubleQuote = this.state.isDoubleQuote;
        var isBraces = this.state.isBraces;
        var isComma = this.state.isComma;
        var guids = '';
        for(var i = 0; i < numberOfGuids; i++){
            var guid = this.guid();
            if(!isHyphens){
                guid = guid.replace(/[^a-zA-Z0-9]/g, "");
            }

            if(isUpperCase){
                guid = guid.toUpperCase();
            }

            if(isDoubleQuote){
                guid = '"' + guid + '"';
            }

            if(isBraces){
                guid = "{" + guid + "}";
            }

            if(isComma){
                guid += ",";
            }

            guids += guid + "\n";
        }      

        this.setState({
            result: guids
        });
    }

    guid = () => (
        this.s4() + this.s4() + '-' + this.s4() + '-' + this.s4() + '-' +
        this.s4() + '-' + this.s4() + this.s4() + this.s4()
    )

    s4 = () => (
        Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1)
    )
    
    render() {
        return (
            <div>
            <div>                
                <div className="col-sm-12 col-md-10 pull-left form-inline">    
                    <h2 className="col-sm-12 col-md-10 pull-left">GUID Generator</h2>

                    <div className="col-sm-12 col-md-12 pull-left buffer-half">
                        Number of GUIDS &nbsp;
                        <input type="number" name="numberOfGuids" 
                            onChange={this.handleInputChange}
                            value={this.state.numberOfGuids}
                            class="form-control"
                        />
                    </div>           
                    <div className="col-sm-12 col-md-12 pull-left buffer-half">
                        <div className="col-sm-3 col-md-2 pull-left zero-padding">
                            <input type="checkbox" name="isUpperCase" 
                                checked={this.state.isUpperCase}
                                onChange={this.handleInputChange} />
                            &nbsp; Uppercase
                        </div>
                        <div className="col-sm-3 col-md-2 pull-left zero-padding">
                            <input type="checkbox" name="isHyphens" 
                                checked={this.state.isHyphens}
                                onChange={this.handleInputChange} />
                                &nbsp; Hyphens
                        </div>
                        <div className="col-sm-3 col-md-2 pull-left zero-padding">
                            <input type="checkbox" name="isBraces" 
                                checked={this.state.isBraces}
                                onChange={this.handleInputChange} />
                                &nbsp; Braces
                        </div>
                    </div>                           
                    <div className="col-sm-12 col-md-12 pull-left buffer-half">
                        <div className="col-sm-3 col-md-2 pull-left zero-padding">
                            <input type="checkbox" name="isComma" 
                                checked={this.state.isComma}
                                onChange={this.handleInputChange} />
                            &nbsp; Comma
                        </div>
                        <div className="col-sm-3 col-md-2 pull-left zero-padding">
                            <input type="checkbox" name="isDoubleQuote" 
                                checked={this.state.isDoubleQuote}
                                onChange={this.handleInputChange} />
                            &nbsp; Double Quote
                        </div>
                    </div>
                    
                    <div className="col-sm-12 col-md-12 pull-left buffer-half">      
                        <button onClick={this.handleClick} className="btn btn-success" type="submit"> Generate GUIDS ! </button>
                    </div>

                    <div className="col-sm-12 pull-left buffer-half">
                        Result <br/>
                        <textarea cols="50" rows="10" 
                            className="font-monospace form-control"
                            name="result" 
                            onChange={this.handleInputChange}
                            value={this.state.result}
                        />
                    </div>
                </div>
            </div>
            
            </div>
        );
    }
}
 
export default GuidGenerator