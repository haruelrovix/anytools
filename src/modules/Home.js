import React from "react";
 
class Home extends React.Component {
  render() {
    return (
      <div>
        <h2>HELLO</h2>
        <p>This is just a simple web application that I hope will help you all.</p>
 
        <p>This app is consist of many utils. I Hope you will enjoy using it</p>
        <a className="margin-side btn btn-primary" href="/guid-generator">Generator</a>
        
        <a className="margin-side btn btn-primary" href="/epoch-converter">Epoch Converter</a>
        <a className="margin-side btn btn-primary" href="/json-xml-converter">JSON - XML Converter</a>
        <a className="margin-side btn btn-primary" href="/speedtest">Internet Speedtest</a>
        <a className="margin-side btn btn-primary" href="/contact">Contact</a>
      </div>
    );
  }
}
 
export default Home