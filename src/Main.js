import React, { Component } from "react";
import {
    Route,
    NavLink,
    BrowserRouter,
}from "react-router-dom"

import Home from "./modules/Home";
import GuidGenerator from "./modules/GuidGenerator";
import EpochConverter from "./modules/EpochConverter";
import JsonXmlConverter from "./modules/JsonXmlConverter";
import Contact from "./modules/Contact";
import Speedtest from "./modules/Speedtest";
 
class Main extends Component {

constructor(props) {
    super(props);
        this.state = {
            name: 'AnyTools',
            date: new Date(),
            search: ''
        };
    }

    handleClick = () => {
        this.movePage(this.state.search)
    }

    movePage = (url) => {
        window.location = url
    }

    handleChange = (e) =>{         
        this.setState({search: e.target.value});
    }

    render() {
        return (
            <BrowserRouter>
            <div>
                <nav className="navbar navbar-inverse zero-radius">
                    <div className="container-fluid">
                        <ul className="nav navbar-nav">
                            <li><NavLink className="any-tools-main-style" to="/">AnyTools</NavLink></li>
                            <li><NavLink to="/guid-generator">Generator</NavLink></li>
                            <li><NavLink to="/epoch-converter">Epoch Converter</NavLink></li>
                            <li><NavLink to="/json-xml-converter">JSON - XML Converter</NavLink></li>
                            <li className="dropdown">
                                <a className="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Converter <b class="caret"></b></a>
                                <ul className="dropdown-menu" role="menu">
                                    <li><NavLink to="/epoch-converter">Epoch Converter</NavLink></li>
                                    <li><NavLink to="/json-xml-converter">JSON - XML Converter</NavLink></li>
                                </ul>
                            </li>                
                            <li><NavLink to="/speedtest">Speedtest</NavLink></li>
                            <li><NavLink to="/contact">Contact</NavLink></li>
                        </ul>
                            <div className="col-sm-3 col-md-3 pull-right">
                            <div className="navbar-form pull-right" role="search">
                                <div className="input-group">
                                    <input type="text" className="form-control" placeholder="Search" name="search" id="srch-term" value={this.state.search} onChange={this.handleChange}></input>
                                    <div className="input-group-btn">
                                        <button onClick={this.handleClick} className="btn btn-default" type="submit"><i className="glyphicon glyphicon-search"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </nav>
                <div className="content">
                    <Route exact path="/" component={Home}/>
                    <Route path="/guid-generator" component={GuidGenerator}/>
                    <Route path="/epoch-converter" component={EpochConverter}/>
                    <Route path="/json-xml-converter" component={JsonXmlConverter}/>
                    <Route path="/contact" component={Contact}/>
                    <Route path="/speedtest" component={Speedtest}/>
                </div>
            </div>
            </BrowserRouter>
        );
    }
}
 
export default Main;